.DEFAULT_GOAL := help
.PHONY: venv help

solve:
	@python3 aoc.py $(filter-out $@, $(MAKECMDGOALS))

%:
	@:

solve-all:
	@python3 aoc.py all

test:
	@python3 -m pytest --cov=days/ --cov=libs/  tests/

bench:
	@python3 bench.py |egrep "solve|seconds"

bench-advanced:
	@scalene bench.py

clean:
	@echo "Remove pycaches and *.pyc file"
	@find . -name '*.pyc' -exec rm -f {} +
	@find . -name '*.pyo' -exec rm -f {} +
	@find . -name '*~' -exec rm -f {} +
	@find . -name '__pycache__' -exec rm -rf {} +

new:
	@touch data/input-$(filter-out $@, $(MAKECMDGOALS)).txt
	@touch days/day$(filter-out $@, $(MAKECMDGOALS)).py
	@touch tests/test_day$(filter-out $@, $(MAKECMDGOALS)).py

help:
	@echo "\t 🎁 🎄 🎅 AdventOfCode 2020 🎅 🎄 🎁 "
	@echo ""
	@echo "solve <day>"
	@echo "\t\tsolve a specific day"
	@echo "solve-all:"
	@echo "\t\trun all solutions of AOC 2020"
	@echo "test:"
	@echo "\t\trun all test and coverage"
	@echo "bench:"
	@echo "\t\tbenchmark the code"
	@echo "bench-advanced:"
	@echo "\t\trun an advanced benchmarch"
	@echo "clean:"
	@echo "\tremove python file"
	@echo ""

