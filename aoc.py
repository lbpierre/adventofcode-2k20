#!/usr/bin/python3
# coding: utf-8

import argparse
from random import choice
from enum import Enum

from days import day1, \
    day2, \
    day3, \
    day4, \
    day5, \
    day6, \
    day7, \
    day8, \
    day9, \
    day10, \
    day11, \
    day12, \
    day13, \
    day14, \
    day15, \
    day16, \
    day17, \
    day18, \
    day19, \
    day20, \
    day21, \
    day22, \
    day23, \
    day24, \
    day25
from libs.common import read_input


def remoji(n: int = 1) -> str:
    return "".join([choice(["🎁", "🎄", "🎅", "🤶",
                            "🦌", "🦃", "🐻‍", "❄"]) for _ in range(n)])


class Colors(Enum):
    BKG_GREEN = "\033[42m"
    BKG_RED = "\033[41m"
    END = "\033[0m"


def solve_all():
    """Solve all days"""

    print(f"{remoji(4)} {Colors.BKG_RED.value}Solve all days! "
          f"{Colors.END.value} {remoji(4)}")

    for d in range(1, 26):
        solve_day(d)

    print(f"🎅 {Colors.BKG_RED.value}Thats all folks{Colors.END.value} 🎁 🎄")


def solve_day(day: int):
    """Solve specific day."""

    days = {
        1: day1.solve,
        2: day2.solve,
        3: day3.solve,
        4: day4.solve,
        5: day5.solve,
        6: day6.solve,
        7: day7.solve,
        8: day8.solve,
        9: day9.solve,
        10: day10.solve,
        11: day11.solve,
        12: day12.solve,
        13: day13.solve,
        14: day14.solve,
        15: day15.solve,
        16: day16.solve,
        17: day17.solve,
        18: day18.solve,
        19: day19.solve,
        20: day20.solve,
        21: day21.solve,
        22: day22.solve,
        23: day23.solve,
        24: day24.solve,
        25: day25.solve,
    }

    def default(_): print(f"> Solution to day {day} not found...")

    solution = days.get(day, default)(read_input(day))

    if solution:
        print(f"Day-{day:>02} -> ⭐ Part-1:"
              f"{Colors.BKG_GREEN.value}{solution.part_1}{Colors.END.value}"
              f" ⭐⭐ Part-2: {Colors.BKG_GREEN.value}{solution.part_2}"
              f"{Colors.END.value} {remoji()}")

    solution


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='AdventOfCode 2k20.')
    parser.add_argument('day', metavar='N', help="day number to solve")

    args = parser.parse_args()
    if args.day == "all":
        solve_all()
    else:
        solve_day(int(args.day))
