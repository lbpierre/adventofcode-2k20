# coding: utf-8

from os import path
from typing import List
from collections import namedtuple
from functools import reduce
from operator import mul

Solution = namedtuple("Solution", ["part_1", "part_2"])


def read_input(day: str, separator: str = '\n') -> List[str]:

    filepath = path.join("data", f"input-{day:0>2}.txt")

    try:
        with open(filepath, 'r') as f:
            return f.read().strip().split("\n")
    except FileNotFoundError:
        return []


def product(iterable: callable) -> int:
    """Return the product of an iterable.
    eg. product([2, 3, 4]) == 2 * 3 * 4 == 24"""
    return reduce(mul, iterable, 1)
