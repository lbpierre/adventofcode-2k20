# coding: utf-8

from days.day18 import solve, \
    compute_line, \
    compute_canonical, \
    sum_line_result
from libs.common import Solution

data = """2 * 3 + ((4 * 5) + 1)
1 + 2 * 3 + 4 * 5 + 6""".split('\n')


def test_compute_canonical():

    assert compute_canonical("42 + 8") == "50"
    assert compute_canonical("51 * 3") == "153"


def test_compute_line():

    assert compute_line("2 * 3 + ((4 * 5) + 1)") == 27
    assert compute_line("1 + 2 * 3 + 4 * 5 + 6") == 71
    assert compute_line("1 + (2 * 3 + 1)") == 8
    assert compute_line("4 + 2 * ((2 + 8 * 7 * 9) * 7 * "
                        "(2 + 4) + (7 + 9 + 5 + 6 * 2 * 6))") == 160704
    assert compute_line("1 + (2 * 3) + (4 * (5 + 6))") == 51
    assert compute_line("((2 + 4 * 9) * (6 + 9 * 8 + 6)"
                        " + 6) + 2 + 4 * 2") == 13632
    assert compute_line("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))") == 12240
    assert compute_line("5 + (8 * 3 + 9 + 3 * 4 * 3)") == 437
    assert compute_line("6 * (2 * 5) * (8 + (5 + 6 * 5 + 6 + 4)"
                        " + (4 + 9 * 9 * 9 * 2 + 2) + 6 + (2 * "
                        "4 + 9 * 8)) * 9 + 4") == 1254424

    assert compute_line("1 + (2 * 3) + (4 * (5 + 6))", True) == 51
    assert compute_line("2 * 3 + (4 * 5)", True) == 46


def test_sum_line_result():

    assert sum_line_result(data) == 98


def test_solveday18():

    assert solve(data) == Solution(98, 279)
