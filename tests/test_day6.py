# coding: utf-8

from days.day6 import solve
from libs.common import Solution

data = """abc

a
b
c

ab
ac

a
a
a
a

b""".split("\n")


def test_solve_day6():

    assert solve(data) == Solution(11, 6)
