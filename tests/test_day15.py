# coding: utf-8

from libs.common import Solution
from days.day15 import play, \
    solve


def test_solve():

    assert play([0, 3, 6], 30) == 3
    assert play([0, 3, 6], 2020) == 436
    assert solve(["0,0,0"]) == Solution(9, 18964)
    # do not run other tests it take too much time
    # day 15 could be improved
