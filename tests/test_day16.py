# coding: utf-8

import types
from days.day16 import solve, \
    extract_rules, \
    get_my_ticket, \
    get_nearby_tickets


data = """class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12""".split('\n')


def prepare_inputs():
    return [line.split('\n') for line in '\n'.join(data).split('\n\n')]


def test_extract_rules():

    _data = prepare_inputs()
    rules = extract_rules(_data[0])
    assert len(rules) == 3

    for rule in rules:
        assert rule.name in ["class", "row", "seat"]
        assert isinstance(rule.validator, types.FunctionType)


def test_get_my_ticket():

    _data = prepare_inputs()
    assert get_my_ticket(_data) == [7, 1, 14]


def test_get_nearby_tickets():

    _data = prepare_inputs()
    assert get_nearby_tickets(_data) == [[7, 3, 47],
                                         [40, 4, 50],
                                         [55, 2, 20],
                                         [38, 6, 12]]


def test_solveday16():

    assert solve(data).part_1 == 71
