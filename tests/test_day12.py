# coding: utf-8

from days.day12 import solve

from libs.common import Solution

data = """F10
N3
F7
R90
F11""".split("\n")



def test_day12():

    assert Solution(25, 286) == solve(data)
