# coding: utf-8

from days.day5 import solve, \
    compute_seat_id

from libs.common import Solution


def test_compute_seat_id():
    assert 357 == compute_seat_id("FBFBBFFRLR")
    assert 567 == compute_seat_id("BFFFBBFRRR")
    assert 119 == compute_seat_id("FFFBBBFRRR")
    assert 820 == compute_seat_id("BBFFBBFRLL")


def test_day5():

    data = """BFFFBBFRRR
FFFBBBFRRR
BBFFBBFRLL""".split("\n")

    # part_2 does not matter here...
    assert solve(data) == Solution(820, 120)

