# coding: utf-8

from libs.common import Solution
from days.day2 import match_passwd_policy, \
    match_new_passwd_policy, \
    parse_policy, \
    Policy, \
    solve


test_data = """1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc""".split("\n")


def test_parse_policy():
    data = list(map(lambda x: x.split(":")[0], test_data))
    assert parse_policy(data[0]) == Policy('a', 1, 3)
    assert parse_policy(data[1]) == Policy('b', 1, 3)
    assert parse_policy(data[2]) == Policy('c', 2, 9)


def test_match_passwd_policy():
    assert match_passwd_policy(Policy('a', 1, 3), "abcde") is True
    assert match_passwd_policy(Policy('b', 1, 3), "cdefg") is False
    assert match_passwd_policy(Policy('c', 2, 9), "ccccccccc") is True


def test_match_new_passwd_policy():
    assert match_new_passwd_policy(Policy('a', 1, 3), "abcde") is True
    assert match_new_passwd_policy(Policy('b', 1, 3), "cdefg") is False
    assert match_new_passwd_policy(Policy('c', 2, 9), "ccccccccc") is False


def test_solve_day2():
    assert solve(test_data) == Solution(2, 1)
