# coding: utf-8

from libs.common import read_input
from libs.common import product


def test_read_input():

    output = read_input("test")

    assert len(output) == 3
    assert output[0] == "123"
    assert output[1] == "456"
    assert output[2] == "789"


def test_read_input_failed():

    assert read_input("this-file-does-not-exit") == []


def test_product():

    assert product([1, 2, 3]) == 6
    assert product([-2, 3, 4]) == -24
    assert product([-1, -2, 3]) == 6
