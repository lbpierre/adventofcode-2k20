# coding: utf-8

from days.day8 import solve
from libs.common import Solution

data = """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6""".split("\n")


def test_solve_day8():

    assert Solution(5, 8) == solve(data)
