# coding: utf-8

from days.day23 import solve
from libs.common import Solution

data = "32415".split("\n")


def test_solveday23():

    assert solve(data) == Solution(5324, 348747561670)
