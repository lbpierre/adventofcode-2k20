# coding: utf-8

from days.day21 import solve
from libs.common import Solution

data = """mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)""".split('\n')


def test_solveday21():

    assert solve(data) == Solution(5, "mxmxvkd,sqjhc,fvjkl")
