# coding: utf-8

from days.day9 import solve
from libs.common import Solution

data = """35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576""".split("\n")


def test_solveday9():

    assert solve(inputs=data, size=5) == Solution(127, 62)
