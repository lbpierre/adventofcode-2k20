# coding: utf-8

from days.day11 import solve
from libs.common import Solution

data = """L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL""".split("\n")


def test_day11():

    assert Solution(37, 26) == solve(data)

