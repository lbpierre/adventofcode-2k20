# coding: utf-8

from libs.common import Solution
from days.day1 import find_candidates, \
    solve


test_data = [
    1721,
    979,
    366,
    299,
    675,
    1456
]


def test_find_candidates():
    assert find_candidates(test_data, 2) == (1721, 299)
    assert find_candidates(test_data, 3) == (979, 366, 675)


def test_solve_day1():
    assert solve(test_data) == Solution(514579, 241861950)
