# coding: utf-8

from libs.common import Solution
from days.day13 import solve, \
    earliest_timestamp

data = """939
7,13,x,x,59,x,31,19""".split("\n")


def test_earliest():
    assert earliest_timestamp("17,x,13,19") == 3417
    assert earliest_timestamp("67,7,59,61") == 754018
    assert earliest_timestamp("67,x,7,59,61") == 779210
    assert earliest_timestamp("67,7,x,59,61") == 1261476
    assert earliest_timestamp("1789,37,47,1889") == 1202161486


def test_solve():
    assert Solution(295, 1068781) == solve(data)
