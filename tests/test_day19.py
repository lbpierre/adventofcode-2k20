# coding: utf-8

from days.day19 import solve
from libs.common import Solution

data = """0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"

ababbb
bababa
abbbab
aaabbb
aaaabbb""".split('\n')


def test_day19():

    assert solve(data) == Solution(2, 2)
