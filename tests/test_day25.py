# coding: utf-8

from days.day25 import solve,\
    guess_loop_size
from libs.common import Solution

data = """5764801
17807724""".split('\n')


def test_guess_loop_size():

    assert guess_loop_size(5764801) == 8


def test_solveday25():

    assert solve(data) == Solution(14897079, 0)
