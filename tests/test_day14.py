# coding: utf-8

from days.day14 import solve, \
    bitmask, \
    x_mask, \
    Memory
from libs.common import Solution


data = """mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1""".split("\n")


def test_x_mask():

    assert x_mask("000000000000000000000000000000X1001X", 42) ==\
        "000000000000000000000000000000X1101X"
    assert x_mask("00000000000000000000000000000000X0XX", 26) ==\
        "00000000000000000000000000000001X0XX"


def test_bitmask():

    mask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X"
    assert bitmask(None, 11, mask) == [Memory(None, 73)]
    assert bitmask(None, 101, mask) == [Memory(None, 101)]
    assert bitmask(None, 0, mask) == [Memory(None, 64)]


def test_day14():
    assert Solution(51, 208) == solve(data)
