# coding: utf-8

from days.day22 import prepare_inputs, \
    play_part_1, \
    play_part_2, \
    score

data_1 = """Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10""".split("\n")

data_2 = """Player 1:
43
19

Player 2:
2
29
14""".split("\n")


def test_solveday22():
    p1, p2 = prepare_inputs(data_1)
    assert play_part_1(p1, p2) == 306
    assert score(max(play_part_2(p1, p2))) == 291
