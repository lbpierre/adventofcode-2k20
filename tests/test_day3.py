# coding: utf-8

from days.day3 import solve
from libs.common import Solution

data = """..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#""".split("\n")


def test_solve_day3():

    assert solve(data) == Solution(7, 336)

