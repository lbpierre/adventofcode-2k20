# coding: utf-8

from days.day17 import solve
from libs.common import Solution

data = """.#.
..#
###""".split('\n')


def test_day17():

    assert solve(data) == Solution(112, 848)
