# coding: utf-8

from days.day10 import solve, find_joltage_one_and_three
from libs.common import Solution

data = """16
10
15
5
1
11
7
19
6
12
4""".split("\n")

data2 = """28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3""".split("\n")

data3 = """4
5
6
7
8
9""".split("\n")


def test_day10():

    def prepare_data(data):
        joltages = lambda d: [int(x) for x in d]
        data = joltages(data)
        data.append(0)
        return sorted(data)

    assert find_joltage_one_and_three(prepare_data(data)) == 35
    assert find_joltage_one_and_three(prepare_data(data2)) == 220
    assert solve(data) == Solution(35, 8)
    assert solve(data2) == Solution(220, 19208)
    assert solve(data3) == Solution(5, 11)
