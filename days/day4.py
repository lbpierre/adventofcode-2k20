# coding: utf-8

import re
from typing import List
from libs.common import Solution


def is_valid_passport(passport: dict) -> bool:
    required_keys = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
    for k in required_keys:
        if k not in passport.keys() and k != "cid":
            return False
    return True


def height_check(s):
    height = re.match(r'^(\d{1,})(cm|in)$', s)
    if height:
        if height[2] == "cm" and 150 <= int(height[1]) <= 193:
            return True
        elif height[2] == "in" and 59 <= int(height[1]) <= 76:
            return True
    return False


def is_valid_passport_advenced(passport: dict) -> bool:
    validation = {
        'byr': lambda s: len(s) == 4 and 1920 <= int(s) <= 2002,
        'iyr': lambda s: len(s) == 4 and 2010 <= int(s) <= 2020,
        'eyr': lambda s: len(s) == 4 and 2020 <= int(s) <= 2030,
        'hgt': height_check,
        'hcl': lambda s: re.match(r'#[a-f0-9]{6}', s),
        'ecl': lambda s: s in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'],
        'pid': lambda s: len(s) == 9 and s.isdigit(),
        'cid': lambda s: True
    }

    for carac, value in passport.items():
        if not validation[carac](value):
            return False
    return True


def parse_passport(raw: str) -> dict:
    return {k: v for k, v in map(lambda x: x.split(":"), raw.split(" "))}


def solve(inputs: List[str]) -> Solution:

    # rearrange inputs (must improve libs.common: read_inputs
    # for next challenge
    inputs = "\n".join(inputs)
    inputs = inputs.split("\n\n")

    part_1, part_2 = 0, 0

    for raw_passport in map(lambda x: " ".join(x.split("\n")), inputs):

        passport = parse_passport(raw_passport)
        if is_valid_passport(passport):
            part_1 += 1

            # Need to check part_2 from filtering of
            # part 1 du ensure key of the dict exist
            if is_valid_passport_advenced(passport):
                part_2 += 1

    return Solution(part_1, part_2)
