# coding: utf-8

from libs.common import Solution
from typing import List


def count_unique(group: str) -> int:
    c = set()
    for i in group.replace("\n", ""):
        c.add(i)
    return len(c)


def count_yes_group(group: str) -> int:
    counter = 0
    solutions = ""
    length_grp = len(group.split("\n"))

    solutions = group.replace("\n", "")
    counted = ""
    for s in solutions:
        cnt = solutions.count(s)
        if cnt == length_grp and s not in counted:
            counter += 1
            counted += s
    return counter


def solve(inputs: List[str]) -> Solution:

    part_1, part_2 = 0, 0

    for group in "\n".join(inputs).split("\n\n"):
        part_1 += count_unique(group)
        part_2 += count_yes_group(group)

    return Solution(part_1, part_2)
