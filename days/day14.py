# coding: utf-8

import re
from typing import List
from libs.common import Solution
from collections import namedtuple

Operation = namedtuple("Operation", ["mask", "address", "value"])
operation_re = re.compile(r"mem\[(?P<address>[0-9]{1,})\]"
                          " = (?P<value>[0-9]{1,})")

Memory = namedtuple("Memory", ["address", "value"])


def read_program(inputs: List[str]):

    mask = ""
    instructions = []
    for instruction in inputs:
        if instruction.startswith("mask"):
            mask = instruction.replace("mask = ", '')
        else:
            match = operation_re.search(instruction)
            instructions.append(Operation(mask=mask,
                                          address=int(match.group("address")),
                                          value=int(match.group("value"))))

    return instructions


def execute_program(program: List[Operation],
                    bitmask_function: callable) -> int:

    memory = {}
    for op in program:
        output = bitmask_function(op.address, op.value, op.mask)
        for _mem in output:
            memory[_mem.address] = _mem.value

    return sum(memory.values())


def aoc_mask(mask: str, value: int) -> int:
    # bitmask_1 apply all 1 of the mask
    bitmask_1 = int(mask.replace('X', '0'), 2)
    # bitmask_2 apply all 0 of the mask
    bitmask_2 = int(mask.replace('X', '1'), 2)

    return (value | bitmask_1) & bitmask_2


def x_mask(mask: str, value: int) -> str:

    value = f"{bin(value).replace('0b', ''):0>36}"
    output = ""
    for mask_chr, value_chr in zip(mask, value):
        if mask_chr == '1':
            output += '1'
        elif mask_chr == '0':
            output += value_chr
        else:
            output += mask_chr

    return output


def bitmask(addr: int, value: int, mask: str) -> List[Memory]:
    return [Memory(address=addr,
                   value=aoc_mask(mask, value))]


def bitmask_x(addr: int, value: int, mask: str) -> List[Memory]:
    """Part-2 merry X-Mask to all of you"""
    addresses = []
    memories = []

    mask = x_mask(mask, addr)

    for x in range(2 ** mask.count('X')):
        _x = f"{bin(x).replace('0b', ''):0>{mask.count('X')}}"
        _mask = ""
        ite_mask = 0
        for j, k in enumerate(mask):
            if mask[j] == 'X':
                _mask += _x[ite_mask]
                ite_mask += 1
            else:
                _mask += k
        addresses.append(_mask)

    for addr in addresses:
        memories.append(Memory(address=addr, value=value))
    return memories


def solve(inputs: List[str]) -> Solution:

    part_1, part_2 = 0, 0
    program = read_program(inputs)
    part_1 = execute_program(program, bitmask)
    part_2 = execute_program(program, bitmask_x)

    return Solution(part_1, part_2)
