# coding: utf-8

from typing import List, \
    Tuple
from functools import reduce
from libs.common import Solution


def bus_timestamp(depart_time: int, shuttles: set) -> int:
    dt = depart_time * depart_time
    bus = 0
    for _bus in shuttles:
        _dt = depart_time - (depart_time % _bus) + _bus
        if dt > _dt:
            bus = _bus
            dt = _dt

    return (dt - depart_time) * bus


def parse_shuttles(inputs: List[str]) -> Tuple:
    bus = set()
    depart_time = int(inputs.pop(0))

    for b in filter(lambda x: x != 'x', inputs[0].split(',')):
        bus.add(int(b))

    return depart_time, bus


def earliest_timestamp(inputs: List[str]) -> List:

    shuttles, shuttles_dt = [], []
    for t, b in enumerate(inputs.split(',')):
        if b == 'x':
            pass
        else:
            dt = int(b)
            shuttles.append(dt)
            shuttles_dt.append(dt - t)

    return chinese_remainder(shuttles, shuttles_dt)


# https://rosettacode.org/wiki/Chinese_remainder_theorem#Python_3.6
def chinese_remainder(n: List[int], a: List[int]) -> int:
    sum = 0
    prod = reduce(lambda a, b: a*b, n)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        sum += a_i * mul_inv(p, n_i) * p
    return sum % prod


def mul_inv(a: int, b: int) -> int:
    b0 = b
    x0, x1 = 0, 1
    if b == 1:
        return 1
    while a > 1:
        q = a // b
        a, b = b, a % b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0:
        x1 += b0
    return x1


def solve(inputs: List[str]) -> Solution:

    part_1, part_2 = 0, 0

    depart_time, bus = parse_shuttles(inputs.copy())
    part_1 = bus_timestamp(depart_time, bus)
    part_2 = earliest_timestamp(inputs[1])
    return Solution(part_1, part_2)
