# coding: utf-8

from typing import List, \
    Tuple, \
    Dict
from libs.common import Solution


def build_universe(inputs: List[str], dimension: int) -> List[Tuple[int, int, int]]:
    """Populate active cubes."""

    universe = []

    for y, line in enumerate(inputs):
        for x, cube in enumerate(line):
            if cube == "#":
                universe.append((x, y) + (0,) * (dimension - 2))

    return universe


def compute_neighbors_vector(dimension: int) -> set:

    neighbors_positions = set()
    xrange = range(-1, 2)

    for x in xrange:
        for y in xrange:
            for z in xrange:
                if dimension == 3:
                    neighbors_positions.add((x, y, z))
                else:
                    for w in xrange:
                        neighbors_positions.add((x, y, z, w))

    return neighbors_positions


def add_cube_and_neighbor(cube, neighbor) -> tuple:
    """Avoid first solution cube[0] + neighbor[1] for all
     coordonate with nasty if dimension..."""

    result = []
    for c, n in zip(cube, neighbor):
        result.append(c + n)

    return tuple(result)


def count_neighbors(universe: List[Tuple], dimension: int = 3) -> List[Tuple]:

    neighbors = {}
    neighbors_positions = compute_neighbors_vector(dimension)

    for cube in universe:
        for neighbor_position in neighbors_positions:
            _neighbor = add_cube_and_neighbor(cube, neighbor_position)
            if _neighbor == cube:
                continue
            if _neighbor not in neighbors:
                neighbors[_neighbor] = 0
            neighbors[_neighbor] += 1

    return neighbors


def apply_conway_rules(universe: List[Tuple],
                       neighbors: Dict) -> List:

    next_active_cubes = []

    for cube in neighbors.keys():
        if cube in universe:
            if 3 >= neighbors[cube] >= 2:
                next_active_cubes.append(cube)
        else:
            if neighbors[cube] == 3:
                next_active_cubes.append(cube)

    return next_active_cubes


def boot(inputs: List[str], dimension: int, cycle: int = 6) -> int:

    universe = build_universe(inputs, dimension)
    for _ in range(cycle):
        neighbors = count_neighbors(universe, dimension)
        universe = apply_conway_rules(universe, neighbors)

    return len(universe)


def solve(inputs: List[str]) -> Solution:

    part_1, part_2 = 0, 0
    part_1 = boot(inputs, 3)
    part_2 = boot(inputs, 4)
    return Solution(part_1, part_2)
