# coding: utf-8

from typing import List,\
    Tuple
from itertools import groupby
from libs.common import Solution
from libs.common import product

# -- REMINDER: to help me keep straight to the solution. --
# Possible path is the production coefficient for futher path...
# all explications are done regarding the
# example 1 puzzle `1,4,5,6,7,10,11,12,15,16,19`, with the rule: max diff of 3.
# 2: strict 2 continues number, eg: 2 continues numbers `10`, `11` here `12`
# can't be taken because its a mandatory path to access `15` (the closest number)
# with those 2 it offers only 2 paths from `10` to reach `15`:
# 1) 10->11->12-15
# 2) 10->12->15
# 3: strict 3 continues number, eg: `4`, `5` and `6`.
# to reach `7` from `4`, those 3 numbers can create 4 sub paths:
# 1) 4->5->6->7
# 2) 4->6->7
# 3) 4->5->7
# 4) 4->7
# New set is required: `1,4,5,6,7,8,10` (this exemple contains only 7 numbers)
# 4: strict 4 continues number position `4` to reach `10` and
# between: `5`, `6`, `7`, `8`; can give 7 sub paths:
# 1) 4->5->6->7->8
# 2) 4->5->7->8
# 3) 4->6->7->8
# 4) 4->6->8
# 5) 4->7->8
# 6) 4->5->8
# 7) 4->6->8


def lazy_caterer_sequence(n: int) -> int:
    """Arithmetic suite: 1-2-4-7-11.."""
    return (n * (n - 1) // 2) + 1


def test_3_in_row(number: int, row: List[int]) -> Tuple[int]:
    """Part1 count diff of 1 and diff of 3."""

    d1, d3 = 0, 0
    for i in row:
        if i - number == 1:
            d1 += 1
            break
        if i - number == 3:
            d3 += 1
            break

    return d1, d3


def find_adapters_paths(joltages: List[int]) -> int:

    subpath_coefficient = []

    def _diff(index: int) -> int:
        """Local function to diff elem X - (X-1)."""
        return joltages[index] - joltages[index - 1]

    # itertools.groupby -> make an iterator that returns consecutive keys
    # and groups from the iterable
    for diff, group in groupby(map(_diff, range(1, len(joltages)))):
        group = list(group)
        group_lenght = len(group)

        # only diff of one count to solve sub paths
        if group_lenght > 1 and diff == 1:
            subpath_coefficient.append(lazy_caterer_sequence(group_lenght))

    return product(subpath_coefficient)


def find_joltage_one_and_three(joltages: List[int]) -> int:

    diff_one, diff_three = 0, 1

    for index_jolt in range(1, len(joltages)):
        curr = test_3_in_row(joltages[index_jolt - 1],
                             joltages[index_jolt: index_jolt + 3])
        diff_one += curr[0]
        diff_three += curr[1]

    return diff_one * diff_three


def solve(inputs: List[str]) -> Solution:

    joltages = [int(x) for x in inputs]
    joltages.append(0)
    joltages = sorted(joltages)
    part_1 = find_joltage_one_and_three(joltages)
    part_2 = find_adapters_paths(joltages)
    return Solution(part_1, part_2)
