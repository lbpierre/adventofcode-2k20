# coding: utf-8

from typing import List
from itertools import cycle
from collections import namedtuple
from libs.common import Solution


# Action N means to move north by the given value.
# Action S means to move south by the given value.
# Action E means to move east by the given value.
# Action W means to move west by the given value.
# Action L means to turn left the given number of degrees.
# Action R means to turn right the given number of degrees.
# Action F means to move forward by the given value in \
# the direction the ship is currently facing.

Instruction = namedtuple("Instruction", ["action", "units"])
CARDINAL = ["E", "S", "W", "N"]


def parse_instruction(instruction: str) -> Instruction:
    """Parse instruction from str input."""
    return Instruction(action=instruction[0],
                       units=int(instruction[1:]))


def get_direction(ship: dict, instruction: Instruction) -> str:
    if instruction.action == "L":
        direction = (CARDINAL.index(ship["direction"]) - (instruction.units // 90)) % 4
    elif instruction.action == "R":
        direction = (CARDINAL.index(ship["direction"]) + (instruction.units // 90)) % 4

    return CARDINAL[direction]


def move_ship(ship: dict, instruction: Instruction) -> dict:

    if instruction.action == "N":
        ship["y"] += instruction.units
    elif instruction.action == "S":
        ship["y"] -= instruction.units
    elif instruction.action == "E":
        ship["x"] += instruction.units
    elif instruction.action == "W":
        ship["x"] -= instruction.units
    elif instruction.action == "L" or instruction.action == "R":
        ship["direction"] = get_direction(ship, instruction)
    elif instruction.action == "F":
        if ship["direction"] == "N":
            ship["y"] += instruction.units
        elif ship["direction"] == "S":
            ship["y"] -= instruction.units
        elif ship["direction"] == "E":
            ship["x"] += instruction.units
        elif ship["direction"] == "W":
            ship["x"] -= instruction.units

    return ship


def move_ship_and_waypoint(ship: dict, instruction: Instruction) -> dict:

    if instruction.action == "N":
        ship["wy"] += instruction.units
    elif instruction.action == "S":
        ship["wy"] -= instruction.units
    elif instruction.action == "E":
        ship["wx"] += instruction.units
    elif instruction.action == "W":
        ship["wx"] -= instruction.units
    elif instruction.action in ("R", "L") and instruction.units == 180:
        ship["wx"], ship["wy"] = -ship["wx"], -ship["wy"]
    elif instruction.action == "R":
        if instruction.units == 90:
            ship["wx"], ship["wy"] = ship["wy"], -ship["wx"]
        elif instruction.units == 270:
            ship["wx"], ship["wy"] = -ship["wy"], ship["wx"]
    elif instruction.action == "L":
        if instruction.units == 90:
            ship["wx"], ship["wy"] = -ship["wy"], ship["wx"]
        elif instruction.units == 270:
            ship["wx"], ship["wy"] = ship["wy"], -ship["wx"]
    elif instruction.action == "F":
        ship["y"] += instruction.units * ship["wy"]
        ship["x"] += instruction.units * ship["wx"]

    return ship


def manathan_distance(ship: dict) -> int:
    return abs(ship["x"]) + abs(ship["y"])


def navigate(inputs: List[str]) -> int:

    ship = {
        "direction": "E",
        "x": 0,
        "y": 0,
    }

    for _instruction in map(parse_instruction, inputs):
        ship = move_ship(ship=ship,
                         instruction=_instruction)

    return manathan_distance(ship)


def navigate_with_waypoint(inputs: List[str]) -> int:

    ship = {
        "direction": "E",
        "x": 0,
        "y": 0,
        "wx": 10,
        "wy": 1
    }

    for _instruction in map(parse_instruction, inputs):
        ship = move_ship_and_waypoint(ship=ship,
                                      instruction=_instruction)

    return manathan_distance(ship)


def solve(inputs: List[str]) -> Solution:

    part_1 = navigate(inputs)
    part_2 = navigate_with_waypoint(inputs)
    return Solution(part_1, part_2)
