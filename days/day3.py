# coding: utf-8

from typing import List
from collections import namedtuple
from libs.common import Solution
from libs.common import product


Grid = namedtuple("Grid", ["cells", "width", "height"])
Slope = namedtuple("Slope", ["dx", "dy"])


def lift_inputs(inputs: List[str]) -> Grid:
    """Prepare the inputs."""

    return Grid(cells=[x.strip() for x in inputs],
                width=len(inputs[0]),
                height=len(inputs))


def slope_it(grid: Grid, slope: Slope) -> int:
    """Count the tree(s) on the slope way."""

    trees, x, y = 0, 0, 0

    for y in range(0, grid.height, slope.dy):
        if grid.cells[y][x] == "#":
            trees += 1

        x += slope.dx
        # use % to avoid building a large table
        # containing X time the inputs
        x %= grid.width

    return trees


def solve(inputs: List[str]) -> Solution:

    grid = lift_inputs(inputs)
    slopes = [Slope(1, 1), Slope(3, 1), Slope(5, 1), Slope(7, 1), Slope(1, 2)]

    part_1 = slope_it(grid, Slope(3, 1))
    part_2 = product(list(map(lambda x: slope_it(grid, x), slopes)))

    return Solution(part_1, part_2)
