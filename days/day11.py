# coding: utf-8

from typing import List
from libs.common import Solution
from collections import namedtuple

# The following rules are applied to every seat simultaneously:
# - If a seat is empty (L) and there are no occupied seats adjacent
#     to it, the seat becomes occupied.
# - If a seat is occupied (#) and four or more seats adjacent to it
#     are also occupied, the seat becomes empty.
# - Otherwise, the seat's state does not change.
# - Floor (.) never changes; seats don't move, and nobody sits on the floor.


Area = namedtuple("Area", ["width", "heigh"])
Position = namedtuple("Position", ["y", "x"])
around_positions = [Position(-1, -1),  # down left
                    Position(-1, 0),   # down
                    Position(-1, 1),   # down right
                    Position(0, -1),   # left
                    Position(0, 1),    # right
                    Position(1, -1),   # upper left
                    Position(1, 0),    # upper
                    Position(1, 1)]   # upper right


def count_occuped_neighbor_seat(seats: List[str],
                                y: int,
                                x: int,
                                closed_neigbours: bool = True) -> int:
    """Count occuped seat around the given position."""

    count = 0

    def _seat_state(seats: List[str], _y: int, _x: int) -> str:
        if _x < 0 or _y < 0:
            # seat does not exist (eg: (-1, 0))
            return ""
        try:
            seat = seats[_y][_x]
        except IndexError:
            return ""
        else:
            return seat

    if closed_neigbours:
        for pos in around_positions:
            if _seat_state(seats=seats, _y=y + pos.y, _x=x + pos.x) == "#":
                count += 1
    else:
        for pos in around_positions:
            offset = 0
            found_sombody = False
            while not found_sombody:
                offset += 1
                state = _seat_state(seats=seats,
                                    _y=(y + offset * pos.y),
                                    _x=(x + offset * pos.x))
                if state == "#":
                    count += 1
                    found_sombody = True
                elif state == "" or state == "L":
                    found_sombody = True

    return count


def run(seats: List[str], closed_neighbors: bool, tolerant: int) -> int:

    area = Area(width=len(seats[0]),
                heigh=len(seats))
    same = False

    while not same:
        new_round = ["" for _ in range(area.width)]
        for y in range(area.heigh):
            for x in range(area.width):
                neighbors = count_occuped_neighbor_seat(seats=seats,
                                                        y=y,
                                                        x=x,
                                                        closed_neigbours=closed_neighbors)
                # print(y, x)
                if seats[y][x] == "L" and neighbors == 0:
                    # seat become occuped
                    new_round[y] += "#"
                elif seats[y][x] == "#" and neighbors >= tolerant:
                    # if over populated area, seat is released
                    new_round[y] += "L"
                else:
                    # same state
                    new_round[y] += seats[y][x]

        if seats != new_round:
            seats = new_round
        else:
            same = False
            break

    return "".join(seats).count("#")


def solve(inputs: List[str]) -> Solution:

    part_1 = run(inputs, True, 4)
    part_2 = run(inputs, False, 5)
    return Solution(part_1, part_2)
