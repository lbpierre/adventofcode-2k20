# coding: utf-8

import re
from typing import List
from operator import mul, add
from functools import reduce
from libs.common import Solution

parentheses_re = re.compile(r"(\([0-9a-f\- *+]{1,}\)){1,}")
operation_re = re.compile("(?P<arg1>[0-9]{1,}) (?P<operator>[+|*]) "
                          "(?P<arg2>[0-9]{1,})")
addition_re = re.compile("(?P<arg1>[0-9]{1,}) (?P<operator>[+]) "
                         "(?P<arg2>[0-9]{1,})")

operations = {
    "*": mul,
    "+": add,
}


def _parenthese_present(x: str) -> bool:
    return True if ')' in x or '(' in x else False


def _addition_present(x: str) -> bool:
    return True if '+' in x else False


def _multiplication_present(x: str) -> bool:
    return True if '*' in x else False


def _operator_present(x: str) -> bool:
    return _multiplication_present(x) or _addition_present(x)


def compute_canonical(expression: str) -> str:
    """Calculate simple expression `x + y` and `x * y`."""
    match = operation_re.match(expression).groupdict()
    return str(reduce(operations.get(match["operator"]), [int(match["arg1"]),
                                                          int(match["arg2"])]))


def reduce_expression(subsystem: str, ordered: bool) -> str:
    """Operation order is addition came first then multiplication."""

    if ordered:
        while _addition_present(subsystem):
            _operation = addition_re.search(subsystem).group()
            subsystem = subsystem.replace(_operation,
                                          compute_canonical(_operation),
                                          1)
    while _operator_present(subsystem):
        _operation = operation_re.search(subsystem).group()
        subsystem = subsystem.replace(_operation,
                                      compute_canonical(_operation),
                                      1)

    return subsystem


def compute_line(system: str, ordered: bool = False) -> int:

    while _parenthese_present(system):
        for parenthese in parentheses_re.findall(system):
            _p = parenthese.replace('(', '', 1).replace(')', '', 1)
            _system = reduce_expression(_p, ordered)
            system = system.replace(parenthese, _system, 1)

    system = reduce_expression(system, ordered)

    return int(system)


def sum_line_result(inputs: List[str], ordered: bool = False) -> int:
    return sum([compute_line(line, ordered) for line in inputs])


def solve(inputs: List[str]) -> Solution:

    part_1 = sum_line_result(inputs)
    part_2 = sum_line_result(inputs, ordered=True)
    return Solution(part_1, part_2)
