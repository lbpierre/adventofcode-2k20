# coding: utf-8

from typing import List
from collections import Counter
from enum import Enum
from libs.common import Solution


class Direction(Enum):
    e = (2, 0)
    w = (-2, 0)
    se = (1, -1)
    sw = (-1, -1)
    ne = (1, 1)
    nw = (-1, 1)


ADJACENT = [(-1, -1),
            (1, -1),
            (2, 0),
            (1, 1),
            (-1, 1),
            (-2, 0)]


def read_path(path: str) -> List[str]:
    """Read each direction from input puzzle."""

    tiles = list()

    for line in path:
        index = 0
        line_length = len(line)
        position = (0, 0)
        while index != line_length:
            _range = 1
            if not (line[index:].startswith(Direction.e.name) or
                    line[index:].startswith(Direction.w.name)):
                _range += 1

            tile = Direction[line[index:index+_range]].value
            position = (position[0] + tile[0], position[1] + tile[1])
            index += _range
        tiles.append(position)

    return tiles


def walk_to_welcome_desk(tiles: List[tuple]):
    black_tiles = 0

    for count in Counter(tiles).values():
        if count % 2:
            black_tiles += 1
    return black_tiles


def live_art(tiles: List[tuple]) -> int:

    _tiles = set()
    for tile, count in Counter(tiles.copy()).items():
        if count % 2:
            _tiles.add(tile)

    for day in range(100):
        black_tiles = set()
        for tile in _tiles:
            count = 0

            # count direct neighbour
            for neighbour in adjacent(tile):
                if neighbour in _tiles:
                    count += 1
                ncount = 0
                # count neighbour of neighbours
                for nneighbour in adjacent(neighbour):
                    if nneighbour in _tiles:
                        ncount += 1
                if ncount == 2 and neighbour not in _tiles:
                    black_tiles.add(neighbour)
            if count == 1 or count == 2:
                black_tiles.add(tile)
        _tiles = black_tiles

    return len(_tiles)


def adjacent(tile: tuple) -> List[tuple]:
    """Returns neighbour positions."""

    _neighbours = []
    for dx, dy in ADJACENT:
        _neighbours.append((dx + tile[0], dy + tile[1]))

    return _neighbours


def solve(inputs: List[str]) -> Solution:

    tiles = read_path(inputs)
    part_1 = walk_to_welcome_desk(tiles)
    part_2 = live_art(tiles)
    return Solution(part_1, part_2)
