# coding: utf-8

import re
from typing import List
from collections import namedtuple
from libs.common import Solution

policy_reg = re.compile("(?P<min>[0-9]{1,})-(?P<max>[0-9]{1,}) "
                        "(?P<char>[a-zA-Z])")

Policy = namedtuple("Policy", ["char", "min", "max"])


def match_passwd_policy(policy: Policy, password: str) -> bool:
    """Policy is 1-3 w where `1` is the minimum occurence
    of character `w` and `3` the maximum occurence of the character
    in the given password"""

    password = password.strip()
    count = password.count(policy.char)
    return True if count <= policy.max and count >= policy.min else False


def match_new_passwd_policy(policy: Policy, password: str) -> bool:
    """Match password policy where policy is:
    first digit the position of the char
    second digit the position of the char
    where first nor second position must match"""

    password = password.strip()
    return True if (password[policy.min - 1] == policy.char) ^ \
        (password[policy.max - 1] == policy.char) else False


def parse_policy(policy: str) -> Policy:
    """Extract password policy from the input string,
    it return a namedtuple Policy object where,
    `char` type is `str`,
    `min` type is `int` and
    `max` type is `int`"""

    match = policy_reg.search(policy)
    return Policy(match.group('char'),
                  int(match.group('min')),
                  int(match.group('max')))


def solve(input: List[str]) -> Solution:
    part_1, part_2 = 0, 0
    for passwd_policy, passwd in map(lambda x: x.split(':'), input):
        passwd_policy = parse_policy(passwd_policy)
        if match_passwd_policy(passwd_policy, passwd):
            part_1 += 1
        if match_new_passwd_policy(passwd_policy, passwd):
            part_2 += 1

    return Solution(part_1, part_2)

