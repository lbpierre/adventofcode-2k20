# coding: utf-8

from typing import List, \
    Tuple
from collections import defaultdict, \
    namedtuple
from libs.common import Solution

Bag = namedtuple("Bag", ["color", "count"])


def parse_bags(inputs: List[str]) -> Tuple[dict, dict]:
    """Create bag as dict."""

    bags, bags_content = defaultdict(set), defaultdict(set)

    for bag in inputs:
        bag_color = " ".join(bag.split(" ")[:2])

        # parse each 'sub' bags
        contents = bag.split("contain ")[1].split(", ")
        for content in contents:
            bag_color2 = " ".join(content.split(" ")[1:3])
            bags[bag_color2].add(bag_color)
            number_of_bags = content.split(" ")[0]

            # manage number of bag as integer instead of
            # string (due to file reading...)
            if number_of_bags == "no":
                # case where no other bags in the given bag
                bags_content[bag_color].add(Bag(bag_color2, 0))
            else:
                # cast value as int
                bags_content[bag_color].add(
                    Bag(bag_color2, int(number_of_bags)))

    return bags, bags_content


def paths_color_bag(color: str,
                    bags: defaultdict,
                    analyzed_bags: set = set()) -> int:

    # loop over list of color from the "reversed" list
    for color in bags[color]:
        # add bags name that are contain in other bag
        analyzed_bags.add(color)

        # inspect its bag to reach back the tree
        paths_color_bag(color=color,
                        bags=bags,
                        analyzed_bags=analyzed_bags)

    return analyzed_bags


def count_bags(color: str, bags: defaultdict) -> int:

    count = 1

    for bag in bags[color]:
        count += bag.count * count_bags(bag.color, bags)

    return count


def solve(inputs: List[str]) -> Solution:

    part_1, part_2 = 0, 0
    bags, bags_content = parse_bags(inputs)

    part_1 = paths_color_bag(color="shiny gold", bags=bags)
    part_2 = count_bags("shiny gold", bags_content)

    return Solution(len(part_1), part_2 - 1)
