# coding: utf-8

import re
from typing import List, \
    Tuple
from collections import defaultdict
from functools import reduce
from libs.common import Solution

foods_re = re.compile(r"(?P<ingredients>[0-9a-zA-Z ]*) "
                      r"\(contains (?P<allergens>[0-9a-z, ]{1,})")


def count_food_using_ingredient(ingredient: str,
                                inputs: List[str]) -> int:

    return len(re.findall(r"\b{}\b".format(ingredient), "".join(inputs)))


def parse_inputs(inputs: List[str]) -> Tuple[set, dict]:

    ingredients = set()
    allergens = defaultdict(list)

    for matchs in map(lambda x: foods_re.search(x).groupdict(), inputs):
        _allergens = matchs.get('allergens').split(',')
        _ingredients = matchs.get('ingredients').split(' ')
        ingredients |= set(_ingredients)
        for allergen in _allergens:
            allergens[allergen.strip()] += [set(_ingredients)]

    return ingredients, allergens


def found_good_food(ingredients: List[str], allergens: dict) -> set:

    ingredients_with_allergens = set()
    for allergen in allergens.values():
        temp = reduce(set.intersection, allergen)
        ingredients_with_allergens |= temp

    safe_foods = ingredients - ingredients_with_allergens
    return safe_foods


def count_foods(foods: set, inputs: List[str]) -> int:
    """Part-1 solution."""
    return sum([count_food_using_ingredient(ingredient, inputs)
                for ingredient in foods])


def compute_dangerous_ingredients(ingredients: List[str], allergens: dict) -> set:

    unsafe = set()
    for allergen, items in allergens.items():
        temp = reduce(set.intersection, items)
        unsafe |= temp
        allergens[allergen] = temp

    foods = set(allergens.keys())

    while foods:
        # pop one ingredient from list of foods
        solo_ingredient = [k for k in foods if len(allergens[k]) == 1][0]
        for allergen in allergens.keys():
            if allergen != solo_ingredient:
                allergens[allergen] -= allergens[solo_ingredient]
        foods.remove(solo_ingredient)

    return allergens


def sort_allergens(food):
    food_list = []
    for name, content in sorted(food.items(), key=lambda x: x[0]):
        food_list.append(list(content)[0])

    return ','.join(food_list)


def solve(inputs: List[str]) -> Solution:

    part_1, part_2 = 0, 0
    ingredients, allergens = parse_inputs(inputs)
    good_foods = found_good_food(ingredients, allergens)
    part_1 = count_foods(good_foods, inputs)
    dangerous_ingredients = compute_dangerous_ingredients(ingredients, allergens)
    part_2 = sort_allergens(dangerous_ingredients)

    return Solution(part_1, part_2)
