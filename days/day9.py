# coding: utf-8

from typing import List
from itertools import combinations
from libs.common import Solution


def is_valid(number: int, preamble: List[int]):
    return True if list(filter(lambda x: sum(x) == number,
                               combinations(preamble, 2))) else False


def xmax_cipher_attack(ciphertext: List[int], size: int = 25) -> int:
    """Part 1."""

    for index in range(size, len(ciphertext)):
        if not is_valid(number=ciphertext[index],
                        preamble=ciphertext[index - size: index]):
            return ciphertext[index]


def xmas_continus_set(ciphertext: List[int],
                      number: int) -> int:
    """Part 2."""

    for _start in range(len(ciphertext)):
        for _end in range(_start + 1, len(ciphertext)):
            _sum = sum(ciphertext[_start: _end])
            if _sum > number:
                # don't compute if sum of a set is already larger than
                # number
                break
            if _sum == number:
                return find_min_max(ciphertext[_start: _end])


def find_min_max(chunck: List[int]) -> int: return min(chunck) + max(chunck)


def solve(inputs: List[str], size: int = 25) -> Solution:

    inputs = list(map(int, inputs))
    part_1 = xmax_cipher_attack(inputs, size)
    part_2 = xmas_continus_set(inputs, part_1)
    return Solution(part_1, part_2)
