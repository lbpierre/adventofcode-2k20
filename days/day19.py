# coding: utf-8

from typing import List,\
    Dict,\
    Tuple
from collections import defaultdict
from libs.common import Solution


def parse_inputs(inputs: List[str]) -> Tuple[Dict[int, list],
                                             List[str]]:
    """Parse input"""
    inputs = ('\n'.join(inputs)).split("\n\n")
    _rules, messages = inputs[0].split('\n'), inputs[1].split('\n')

    rules = defaultdict(list)
    for rule_id, rule_content in map(lambda x: x.split(': '), _rules):
        if any(map(lambda x: "a" in x or "b" in x, rule_content)):
            rules[int(rule_id)] = rule_content.replace('"', '')
        else:
            for subrules in rule_content.split(' | '):
                rules[int(rule_id)].append([int(subrule_id) for subrule_id
                                            in subrules.split(' ')])

    return rules, messages


def resolution(rules: Dict,
               rule_id: int,
               message: str,
               index: int) -> set:

    rule = rules.get(rule_id)
    parsed = set()

    if 'a' in rule or 'b' in rule:
        if index < len(message) and rule[0] == message[index]:
            # if it match the current position as valid `b` or `a`
            # continue the rule verification
            return {index + 1}
        else:
            # otherwise return empty set
            return set()

    for subrules in rule:
        _index_rule_checked = {index}
        # mandatory due rules with `|`
        for _rule in subrules:
            _index_subrule_checked = set()
            for _index in _index_rule_checked:
                # update a set with: setA |= setB
                _index_subrule_checked |= resolution(rules, _rule, message, _index)
            _index_rule_checked = _index_subrule_checked
        parsed |= _index_rule_checked

    return parsed


def part_x_solution(rules: Dict, messages: List[str]) -> int:

    out = []

    for message in messages:
        parsed = resolution(rules, 0, message, 0)
        if len(message) in parsed:
            out.append(1)

    return sum(out)


def solve(inputs: List[str]) -> Solution:

    rules, messages = parse_inputs(inputs)

    part_1 = part_x_solution(rules, messages)
    rules[8] = [[42], [42, 8]]
    rules[11] = [[42, 31], [42, 11, 31]]
    part_2 = part_x_solution(rules, messages)

    return Solution(part_1, part_2)
