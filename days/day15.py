# coding: utf-8

from typing import List
from libs.common import Solution

# https://oeis.org/A181391
# Van Eck's sequence
# Improvement:
# https://www.reddit.com/r/math/comments/dbdhpj/i_found_something_kind_of_cool_about_van_ecks/


def play(inputs: List[int], iteration: int) -> int:

    tape = {v: i for i, v in enumerate(inputs)}
    index = 0

    for i in range(len(inputs), iteration-1):
        tape[index], index = i, i - tape.get(index, i)

    return index


def solve(inputs: List[str]) -> Solution:

    inputs = [int(i) for i in inputs[0].split(',')]
    part_1 = play(inputs, 2020)
    part_2 = play(inputs, 30000000)
    return Solution(part_1, part_2)
