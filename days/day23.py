# coding: utf-8

from typing import List
from libs.common import Solution


def play(cups: List[int], rounds: int = 100) -> List[int]:

    def pickup(cups, current):

        idx = cups.index(current) + 1
        # must take idx modulo len(cups) for outbounded list item

        picked, cups[idx:idx+3] = cups[idx:idx+3], []
        out = 3 - len(picked)
        if out:
            picked += cups[:out]
            cups[:out] = []
        return picked

    current = cups[0]
    max_cup = max(cups)

    for _ in range(rounds):

        # Pick up three cups clockwise
        picked = pickup(cups, current)

        # destination cup: value of current cup - 1
        destination = current - 1 or max_cup

        # if destination value is in the picked up cups
        # it decrease the dest value
        while destination in picked:
            destination = destination - 1 or max_cup

        # puzzle.remove(destination)
        # re-arrange the cups list
        _idx = cups.index(destination) + 1
        cups[_idx:_idx] = picked

        # update index
        current = cups[(cups.index(current) + 1) % len(cups)]

    return cups


def play_part_2(cups: List[int], rounds: int = 10**7):
    """recode entirely, compuation time with first
    solution is above 1hour..."""

    mem = {n: shifted for n, shifted in zip(cups, cups[1:] + [cups[0]])}
    current = cups[0]

    for idx in range(rounds):
        idx = current
        pickup = []

        # pick up 3 cups
        for _ in range(3):
            pickup.append(mem[idx])
            idx = mem[idx]

        cup = current - 1
        i = 1
        while True:
            cup = current-i if cup > 0 else len(cups)+cup
            if cup not in pickup:
                break
            i += 1

        if cup <= 0:
            cup = len(cups) + cup

        # re-arragne cups
        destination = cup

        temp0 = mem[pickup[-1]]
        temp1 = mem[destination]
        temp2 = mem[current]
        mem[current] = temp0
        mem[pickup[-1]] = temp1
        mem[destination] = temp2

        current = mem[current]

    idx = 1
    out = []
    for _ in cups:
        idx = mem[idx]
        out.append(idx)

    # re-arragne outup in right order
    return out


def score_part_1(cups):
    """Nasty score computation."""
    return int("".join([str(i) for i in cups[cups.index(1) + 1:]]) +
               "".join([str(i) for i in cups[:cups.index(1)]]))


def score_part_2(cups):
    return cups[0] * cups[1]


def solve(inputs: List[int]) -> Solution:

    puzzle = list(map(int, inputs[0]))
    part_1 = score_part_1(play(puzzle.copy(), 100))

    puzzle = puzzle + list(range(len(puzzle) + 1, 10**6 + 1))
    part_2 = score_part_2(play_part_2(puzzle, 10**7))

    return Solution(part_1, part_2)
