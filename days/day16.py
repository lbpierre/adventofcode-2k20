# coding: utf-8

import re
from typing import List,\
    Dict
from collections import namedtuple
from libs.common import Solution
from libs.common import product

re_rule = re.compile("(?P<name>[a-zA-Z0-9 ]{1,}):"
                     " (?P<min_r1>[0-9]{1,})-(?P<max_r1>[0-9]{1,}) or"
                     " (?P<min_r2>[0-9]{1,})-(?P<max_r2>[0-9]{1,})")


# name is string and validator is callable
Rule = namedtuple("Rule", ["name", "validator"])
# state is a boolean and value an integer
Status = namedtuple("Status", ["state", "value"])


def valid_function(min_r1: int, max_r1: int,
                   min_r2: int, max_r2: int) -> callable:

    def validator(ticket: int) -> int:
        """Return 0 if ticket is valid else the ticket number."""

        if min_r1 <= ticket and ticket <= max_r1: return 0 # noqa
        if min_r2 <= ticket and ticket <= max_r2: return 0 # noqa

        return ticket

    return validator


def extract_rules(inputs: List[str]) -> Dict[str, callable]:
    """Extract ticket rules."""

    rules = []
    for match in map(re_rule.search, inputs):
        rules.append(Rule(name=match.group('name'),
                          validator=valid_function(min_r1=int(match.group('min_r1')),
                                                   max_r1=int(match.group('max_r1')),
                                                   min_r2=int(match.group('min_r2')),
                                                   max_r2=int(match.group('max_r2')))))

    return rules


def get_my_ticket(inputs: List[str]) -> List[int]:
    return [int(field) for field in inputs[1][1].split(',')]


def get_nearby_tickets(inputs: List[str]) -> List[List[int]]:

    inputs[2].pop(0)  # remove first string: `nearby ticket:`
    tickets = []

    for ticket in map(lambda x: x.split(','), inputs[2]):
        tickets.append([int(field) for field in ticket])

    return tickets


def compute_error_rate(rules: List[Rule], tickets: List[List[int]]) -> int:
    """Error rate is the sum of invalid fields for all tickets."""

    error_rate = 0
    for ticket in tickets.copy():
        for field in ticket:
            if all(map(lambda x: x(field), [r.validator for r in rules])):
                error_rate += field
                tickets.remove(ticket)
                break

    return error_rate, tickets


def set_fields(rules: List[Rule], tickets: List[int]) -> List[Rule]:

    candidats = {index: {r.name for r in rules}
                 for index in range(len(tickets[0]))}
    # candidats are sorted as {0: {"field x", "field y"},
    #                         1: {"field z", "field k"}, etc..}

    for ticket in tickets:
        for index, field in enumerate(ticket):
            for rule in rules:
                # remove from candidat index X the rule name's
                # only if rule does not work for this field
                if rule.validator(field) and (rule.name in candidats[index]):
                    candidats[index].discard(rule.name)

    # Second filtering for field that have more than one rule!
    all_field_for_one_rule = True
    while all_field_for_one_rule:
        all_field_for_one_rule = False
        for index, _rules in candidats.items():
            # remove rule name for other candidat when a candidat
            # already match its rule
            if len(_rules) == 1:
                for _index in candidats.keys():
                    if _index == index:
                        continue
                    candidats[_index] = candidats[_index] - _rules

            # do not quit until all field have one and only one rule
            if not all(map(lambda x: len(x) == 1, candidats.values())):
                all_field_for_one_rule = True

    # flatten the dict: {1: {"rule A"}, etc...} => {1: "rule A"}
    # because each values is a set of one element
    return {k: v.pop() for k, v in candidats.items()}


def compute_departures(candidats: dict, ticket: List[int]) -> int:

    accu = [1]

    for index, value in filter(
            lambda candidat: candidat[1].startswith("departure "),
            candidats.items()):

        accu.append(ticket[index])

    return product(accu)


def solve(inputs: List[str]) -> Solution:

    # rearange lines by section (class, thden your ticket and other tickets
    inputs = [line.split('\n') for line in '\n'.join(inputs).split('\n\n')]
    rules = extract_rules(inputs[0])
    my_ticket = get_my_ticket(inputs)
    nearby_tickets = get_nearby_tickets(inputs)

    part_1, nearby_tickets_cleaned = compute_error_rate(rules, nearby_tickets)
    candidats = set_fields(rules, nearby_tickets_cleaned)
    part_2 = compute_departures(candidats, my_ticket)

    return Solution(part_1, part_2)
