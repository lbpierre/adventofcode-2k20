# coding: utf-8

from typing import List
from libs.common import Solution
from libs.common import product
from itertools import permutations


def find_candidates(inputs: List[str], N_candidats: int) -> int:
    """Find N number which sum's is 2020
    inputs: is the list of integer in the file
    candidats: number of candidat that meet whose add equal to 2020"""

    for candidats in filter(lambda m: sum(m) == 2020,
                            permutations(inputs, N_candidats)):
        return candidats


def solve(inputs: List[str]) -> Solution:

    # some inputs lifting
    inputs = list(map(int, inputs))

    part_1 = product(find_candidates(inputs, 2))
    part_2 = product(find_candidates(inputs, 3))

    return Solution(part_1, part_2)
