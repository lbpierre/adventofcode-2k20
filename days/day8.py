# coding: utf-8

from typing import List
from collections import namedtuple
from libs.common import Solution

Status = namedtuple("Status", ["state", "value"])


def exec_boot_code(boot_code: List[str]) -> Status:

    accumulator = 0
    eip = 0
    eip_history = []

    while True:

        if eip in eip_history:
            return Status("Failed", accumulator)
        if eip == len(boot_code):
            return Status("Success", accumulator)

        opcode = boot_code[eip].split()
        eip_history.append(eip)

        if opcode[0] == "nop":
            eip += 1
        elif opcode[0] == "acc":
            accumulator += int(opcode[1])
            eip += 1
        elif opcode[0] == "jmp":
            eip += int(opcode[1])
        else:
            raise Exception(f"Invalid instruction: {opcode}")


def find_corrupted_instruction(boot_code: List[str]) -> int:

    for pos, opcode in enumerate(map(str.split, boot_code)):
        boot_code_to_test = boot_code.copy()
        changed = False
        if opcode[0] == "jmp":
            changed = True
            boot_code_to_test[pos] = f"nop {opcode[1]}"
        elif opcode[0] == "nop":
            changed = True
            boot_code_to_test[pos] = f"jmp {opcode[1]}"

        if changed:
            status = exec_boot_code(boot_code_to_test)
            if status.state == "Success":
                return status


def solve(inputs: List[str]) -> Solution:
    part_1 = exec_boot_code(inputs)
    part_2 = find_corrupted_instruction(inputs)
    return Solution(part_1.value, part_2.value)
