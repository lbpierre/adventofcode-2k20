# coding: utf-8

import re
from copy import deepcopy
from typing import List

from libs.common import Solution
from libs.common import product

"""--- Day 20: Jurassic Jigsaw ---

The high-speed train leaves the forest and quickly carries you south.
You can even see a desert in the distance! Since you have some spare
 time, you might as well see if there was anything interesting in the
 image the Mythical Information Bureau satellite captured.

After decoding the satellite messages, you discover that the data actually
contains many small images created by the satellite's camera array.
The camera array consists of many cameras; rather than produce a single
 square image, they produce many smaller square image tiles that need to
 be reassembled back into a single image.

Each camera in the camera array returns a single monochrome image tile
with a random unique ID number. The tiles (your puzzle input)
arrived in a random order.

Worse yet, the camera array appears to be malfunctioning:
 each image tile has been rotated and flipped to a random orientation.
Your first task is to reassemble the original image by
 orienting the tiles so they fit together.

To show how the tiles should be reassembled, each tile's image
data includes a border that should line up exactly with its adjacent tiles.
All tiles have this border, and the border lines up exactly when the tiles
 are both oriented correctly. Tiles at the edge of the image also have this
 border, but the outermost edges won't line up with any other tiles.
"""

tile_id_re = re.compile(r"Tile (?P<_id>\d+):", re.ASCII)


class SeaMonster:

    """el famoso sea monster is a static class"""

    shape = """                  # 
#    ##    ##    ###
 #  #  #  #  #  #   """  # noqa

    def __init__(self):
        self.heigh = len(self.shape.split("\n"))
        self.width = len(self.shape.split("\n")[0])
        self.body = self.__build_body()

    def __build_body(self):
        body = []
        for row_index, row in enumerate(self.shape.split("\n")):
            for cell_index, cell in enumerate(row):
                if cell == "#":
                    body.append((cell_index, row_index))
        return body


class Tile:

    """Represent a tile according to AoC statement, it allow 
    tile rotation, flipping"""

    def __init__(self, _id: int, content: List[str] = []):
        self.id = _id
        self.content = content
        self.permutations = self.permutated()
        self.neighbours = dict()
        self.operations = list()

    @property
    def edges(self) -> dict:
        return {
            "up": self.content[0],
            "down": self.content[-1][::-1],
            "left": "".join(list(map(lambda x: x[0], self.content)))[::-1],
            "right": "".join(list(map(lambda x: x[-1], self.content)))
        }

    def edge_options(self, edge: str) -> List[str]:
        """return possible value for a given edge """
        return [self.edges.get(edge), self.edges.get(edge)[::-1]]

    def permutated(self) -> dict:
        """Only permutated edges"""

        permutations = self.edges.copy()
        permutations.update(
            {f"f{pos}": "".join(edge[::-1]) for pos, edge in self.edges.items()})
        return permutations

    def __contains__(self, edge: List[str]) -> bool:
        """Overwrite `in` class operator to check if
        edge is a possible edges"""

        return edge in self.permutations.values()

    def add_neighbour(self, tile, edge: str) -> None:
        self.neighbours[tile] = edge

    def rotate(self) -> None:
        """rotate by 90°"""

        row = len(self.content)
        column = len(self.content[0])
        rotated_tile = [["x" for _ in range(column)] for _ in range(row)]
        for _row in range(row):
            for _column in range(column):
                rotated_tile[_row][_column] = self.content[column - _column - 1][_row]

        self.content = ["".join(r) for r in rotated_tile]

    def flip(self) -> None:
        """Flip on Y axe """

        self.content = self.content[::-1]


class Camera:

    """The camera is a bunch of tile that are not in their correct order and
     state (rotated and/or flipped)"""

    def __init__(self, tiles: List[Tile]):
        self.tiles = tiles
        self.image = []
        self.picture = []
        self.grid = {}
        self.column = 0
        self.row = 0
        self.sea_monster = SeaMonster()
        self.monster = set()

    def prepare_picture(self) -> None:
        """ 'Align tiles line and row in a array of string"""

        tile_lenght = len(list(self.tiles.values())[0].content)

        for tile_index in range(len(self.image) // tile_lenght):
            self.picture.extend(
                self.image[(tile_lenght * tile_index) + 1:
                           (tile_lenght * tile_index) + 9])

        _picture = []
        for row in self.picture:
            temp_row = []
            for column in range(len(row) // tile_lenght):
                temp_row.append(row[
                    (tile_lenght * column) + 1:
                    (tile_lenght * column) + 9])

            _picture.append("".join(temp_row))

        # print("\n".join(_picture)) # this display the full picture
        self.picture = _picture

    def get_pixel(self, y: int, x: int) -> str:
        """return pixel of the picture at the given coordonates """
        return self.picture[y][x]

    def flip(self):
        """Flip the picture"""
        self.picture = self.picture[::-1]

    def rotate(self):
        """Rotate all the picture"""
        rotated = [["" * len(self.picture)] * len(self.picture[0])]
        for row in range(len(self.picture)):
            for cell in range(len(self.picture[0])):
                rotated[row][cell] = self.picture[len(self.picture[0]) - cell - 1][row]

        self.picture = ["".join(row) for row in rotated]

    def find_neighbours(self):
        """Double loop lockup to match tiles edges."""

        for tile in self.tiles.values():
            for _tile in self.tiles.values():
                if _tile.id == tile.id:
                    continue

                # for each edge of _tile check if it
                # matchs its current tile permutated edges
                for permutation in _tile.permutations.values():
                    for edge in tile.edges.values():
                        if edge == permutation:
                            tile.add_neighbour(_tile, edge)

    def add_tile(self, tile: Tile, new_row: bool = False) -> None:
        """Work because moving tile from top left corner."""

        if self.image == []:
            # ensure add tile is in charge
            # of initialization of image
            self.image = deepcopy(tile.content)
        elif new_row:
            for row in tile.content:
                self.image.append(row)
        else:
            _row = len(self.image) - len(tile.content)
            for row in range(len(tile.content)):
                self.image[_row + row] += tile.content[row]

    def build_image(self):
        """Rebuild image according to the neighbours."""

        # 0) list corners tile by filtering on tile with only two neighbours
        corners = list(filter(lambda tile: len(tile.neighbours) == 2, self.tiles.values()))

        # 1) Find and place a corner in its "standard" orientation
        tile = corners[0]
        neighbours = list(tile.neighbours.values())
        neighbours += [neighbour[::-1] for neighbour in neighbours]

        while True:
            down, right = tile.edges.get('down'), tile.edges.get('right')
            if down in neighbours and right in neighbours:
                break
            tile.rotate()

        self.grid[(self.row, self.column)] = tile
        self.add_tile(tile)
        self.column += 1

        # 2) Build image row by row
        while len(self.grid) != len(self.tiles):

            if self.column == 0:
                previous_tile = self.grid[(self.row - 1, self.column)]
                edge_options = previous_tile.edge_options("down")
                tile_id = [neighbour.id for neighbour, edge in previous_tile.neighbours.items()
                           if edge in edge_options][0]
                tile = self.tiles[tile_id]

                self.__orientes_tile(tile, previous_tile, "up", "down")
                self.grid[(self.row, self.column)] = tile
                self.add_tile(tile, new_row=True)
                self.column += 1
            else:
                previous_tile = tile
                edge_options = previous_tile.edge_options("right")
                tile_ids = [neighbour.id for neighbour, edge in previous_tile.neighbours.items()
                            if edge in edge_options]

                if len(tile_ids) == 1:
                    tile_id = tile_ids[0]
                    tile = self.tiles[tile_id]
                    self.__orientes_tile(tile, previous_tile, "left", "right")
                    self.grid[(self.row, self.column)] = tile
                    self.add_tile(tile)
                    self.column += 1
                elif len(tile_ids) == 0:
                    # if no more neighbour for the current tile
                    # then move to next row
                    self.row, self.column = self.row + 1, 0

        # 2) Rebuild all the image according to the grid object
        self.prepare_picture()

    def __orientes_tile(self, tile: Tile, previous_tile: Tile,
                        tile_edge: str, previous_tile_edge: str) -> None:
        """Rotate and flip the current tile until its edge fit previous edge
        Rotate and Flip the camera (8 possible arrays)
        Possible states are: None, flip(x) | flip(y)| rot([90| 180| 270])|
                             and flip(x) + rot(90)| flip(y) + rot(90)
        Those 4 below are simplification
        -> flip(x) + rot(270) = rot(90)
        -> flip(y) + rot(270) = rot(180)
        -> flip(x) + rot(180) = flip(y)
        -> flip(y) + rot(180) = flip(x)"""

        for orientation in range(8):
            if orientation == 4:
                tile.flip()
            if tile.edges.get(tile_edge) == previous_tile.edges.get(previous_tile_edge)[::-1]:
                return
            tile.rotate()

    def find_monster(self):
        """search for each starting position in the picture
        search for the sea monster"""

        for orientation in range(8):
            if orientation == 4:
                self.flip()
            for row in range(len(self.picture) - self.sea_monster.heigh - 1):
                for column in range(len(self.picture[0]) - self.sea_monster.width):
                    if all([self.get_pixel(row + monster_row, column + monster_column) == "#" for monster_column, monster_row in self.sea_monster.body]):
                        for monster_column, monster_row in self.sea_monster.body:
                            self.monster.add((row + monster_row, column + monster_column))

    def part_1(self):
        """find tiles with 2 neighbors and multiple their ID."""

        return product(map(lambda tile: tile.id,
                           filter(lambda t: len(t.neighbours) == 2,
                                  self.tiles.values())))

    def part_2(self):
        """Count the number of #"""
        return len([cell for row in self.picture for cell in row if cell == "#"]) - len(self.monster)


def prepare_inputs(inputs: List[str]) -> Camera:
    """Extract tile as tile_id and a list of str."""

    inputs = ('\n'.join(inputs)).split("\n\n")

    tiles = {}

    for _tile in map(lambda x: x.split('\n'), inputs):
        tile_id = tile_id_re.match(_tile.pop(0))
        tile = Tile(int(tile_id.group('_id')), content=_tile)
        tiles[tile.id] = tile

    camera = Camera(tiles=tiles)

    return camera


def solve(inputs: List[str]) -> Solution:

    part_1, part_2 = 0, 0
    camera = prepare_inputs(inputs)
    camera.find_neighbours()
    part_1 = camera.part_1()
    camera.build_image()
    camera.find_monster()
    part_2 = camera.part_2()

    return Solution(part_1, part_2)
