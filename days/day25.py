# coding: utf-8

from typing import List
from libs.common import Solution

"""
The handshake used by the card and the door involves an operation
that transforms a subject number. To transform a subject number,
start with the value 1. Then, a number of times called the loop size,
perform the following steps:
 - Set the value to itself multiplied by the subject number.
 - Set the value to the remainder after dividing the value by 20201227.

The card always uses a specific, secret loop size when it transforms a
subject number. The door always uses a different, secret loop size.

The cryptographic handshake works like this:
- The card transforms the subject number of 7 according to the card's
    secret loop size. The result is called the card's public key.
- The door transforms the subject number of 7 according to the door's
    secret loop size. The result is called the door's public key.
- The card and door use the wireless RFID signal to transmit the two
    public keys (your puzzle input) to the other device. Now, the card has the
    door's public key, and the door has the card's public key. Because you can
    eavesdrop on the signal, you have both public keys, but neither
    device's loop size. 
- The card transforms the subject number of the door's
    public key according to the card's loop size. The result is the
    encryption key.
- The door transforms the subject number of the card's public key according
    to the door's loop size. The result is the same encryption key as 
    the card calculated.

[...]

At this point, you can use either device's loop size with the other
 device's public key to calculate the encryption key.
"""


def transformation(iv: int, subject_number: int = 7) -> int:
    """Single round of transformation."""
    return (iv * subject_number) % 20201227


def guess_loop_size(public_key: int) -> int:

    iv = 1
    loop_size = 0
    while iv != public_key:
        iv = transformation(iv)
        loop_size += 1

    return loop_size


def handshake_operation(loop_size: int, subject_number: int):

    iv = 1
    for _ in range(loop_size):
        iv = transformation(iv, subject_number)
    return iv


def find_encryption_key(card_public_key: int, door_public_key: int) -> int:

    loop_size_card = guess_loop_size(card_public_key)
    private_key = handshake_operation(loop_size_card, door_public_key)

    return private_key


def solve(inputs: List[str]) -> Solution:

    public_keys = list(map(int, inputs))
    card_public_key = public_keys[0]
    door_public_key = public_keys[1]

    part_1 = find_encryption_key(card_public_key, door_public_key)

    return Solution(part_1, 0)
