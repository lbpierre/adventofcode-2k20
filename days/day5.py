# coding: utf-8

from typing import List
from libs.common import Solution

# Regarding the seat_id computatation: column * 8 + row
# seat id are a "binary" representation of the FBFBF LR.. format


def compute_seat_id(bs: str) -> int:
    """Convert FLBR to binary form """
    binary_value = bs.replace('B', '1').\
        replace('R', '1').\
        replace('L', '0').\
        replace('F', '0')
    return int(binary_value, 2)


def solve(inputs: List[str]) -> Solution:

    part_1, part_2 = 0, 0
    seats_id = set()

    for bording in inputs:
        seats_id.add(compute_seat_id(bording))

    part_1 = max(seats_id)

    for seat in range(min(seats_id), max(seats_id)):
        if seat not in seats_id:
            part_2 = seat
            break

    return Solution(part_1, part_2)

