# coding: utf-8

from typing import List, \
    Tuple
from libs.common import Solution


def play_part_1(player1: List[int], player2: List[int]) -> int:

    # performs a copy to avoid altering original
    # player cards set
    _player1 = player1.copy()
    _player2 = player2.copy()

    while _player1 and _player2:
        player1_card = _player1.pop(0)
        player2_card = _player2.pop(0)

        if player1_card > player2_card:
            _player1.extend([player1_card, player2_card])
        else:
            _player2.extend([player2_card, player1_card])

    return score(_player1) or score(_player2)


def play_part_2(player1: List[int], player2: List[int]) -> int:

    mem = set()

    while player1 and player2:

        seen = (tuple(player1), tuple(player2))
        if seen in mem:
            return player1, []

        mem.add(seen)
        player1_card = player1.pop(0)
        player2_card = player2.pop(0)

        if len(player1) >= player1_card and len(player2) >= player2_card:
            subp1 = player1[:player1_card]
            subp2 = player2[:player2_card]
            s1, s2 = play_part_2(subp1, subp2)
            if (s1 > s2):
                player1.extend([player1_card, player2_card])
            else:
                player2.extend([player2_card, player1_card])
            # avoid last part of the while
            continue

        if player1_card > player2_card:
            player1.extend([player1_card, player2_card])
        else:
            player2.extend([player2_card, player1_card])

    return player1, player2


def prepare_inputs(inputs: List[str]) -> Tuple[List[int]]:
    """Return players cards as list of integer."""

    player2_index = inputs.index("Player 2:")
    player1 = inputs[1: player2_index - 1]
    player2 = inputs[player2_index + 1:]

    def _intify(x): return list(map(int, x))

    return _intify(player1), _intify(player2)


def score(cards):
    """Simply apply wining score calcul: In reverse order of
    the player cards, multiply card value by its index (rev order)."""

    total = 0
    for idx, card_value in enumerate(cards[::-1], start=1):
        total += idx * card_value

    return total


def solve(inputs: List[str]) -> Solution:

    p1, p2 = prepare_inputs(inputs)

    part_1 = play_part_1(p1, p2)
    part_2 = score(max(play_part_2(p1, p2)))

    return Solution(part_1, part_2)
