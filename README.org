* AdventOfCode 2k20
** Solution
| Day                             | Statement | Solution |
|---------------------------------+-----------+----------|
| Day 1: Report Repair            | [[https://adventofcode.com/2020/day/1][AoC 1]]     | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day1.py][day 1]]    |
| Day 2: Password Philosophy      | [[https://adventofcode.com/2020/day/2][AoC 2]]     | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day2.py][day 2]]    |
| Day 3: Toboggan Trajectory      | [[https://adventofcode.com/2020/day/3][AoC 3]]     | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day3.py][day 3]]    |
| Day 4: Passport Processing      | [[https://adventofcode.com/2020/day/4][AoC 4]]     | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day4.py][day 4]]    |
| Day 5: Binary Boarding          | [[https://adventofcode.com/2020/day/5][AoC 5]]     | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day5.py][day 5]]    |
| Day 6: Custom Customs           | [[https://adventofcode.com/2020/day/6][AoC 6]]     | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day6.py][day 6]]    |
| Day 7: Handy Haversacks         | [[https://adventofcode.com/2020/day/7][AoC 7]]     | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day7.py][day 7]]    |
| Day 8: Handheld Halting         | [[https://adventofcode.com/2020/day/8][AoC 8]]     | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day8.py][day 8]]    |
| Day 9: Encoding Error           | [[https://adventofcode.com/2020/day/9][AoC 9]]     | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day9.py][day 9]]    |
| Day 10: Adapter Array           | [[https://adventofcode.com/2020/day/10][AoC 10]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day10.py][day 10]]   |
| Day 11: Seating System          | [[https://adventofcode.com/2020/day/11][AoC 11]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day11.py][day 11]]   |
| Day 12: Rain Risk               | [[https://adventofcode.com/2020/day/12][AoC 12]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day12.py][day 12]]   |
| Day 13: Shuttle Search          | [[https://adventofcode.com/2020/day/13][AoC 13]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day13.py][day 13]]   |
| Day 14: Docking Data            | [[https://adventofcode.com/2020/day/14][AoC 14]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day14.py][day 14]]   |
| Day 15: Rambunctious Recitation | [[https://adventofcode.com/2020/day/15][AoC 15]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day15.py][day 15]]   |
| Day 16: Ticket Translation      | [[https://adventofcode.com/2020/day/16][AoC 16]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day16.py][day 16]]   |
| Day 17: Conway Cubes            | [[https://adventofcode.com/2020/day/17][AoC 17]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day17.py][day 17]]   |
| Day 18: Operation Order         | [[https://adventofcode.com/2020/day/18][AoC 18]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day18.py][day 18]]   |
| Day 19: Monster Messages        | [[https://adventofcode.com/2020/day/19][AoC 19]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day19.py][day 19]]   |
| Day 20: Jurassic Jigsaw         | [[https://adventofcode.com/2020/day/20][AoC 20]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day20.py][day 20]]   |
| Day 21: Allergen Assessment     | [[https://adventofcode.com/2020/day/21][AoC 21]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day21.py][day 21]]   |
| Day 22: Crab Combat             | [[https://adventofcode.com/2020/day/22][AoC 22]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day22.py][day 22]]   |
| Day 23: Crab Cups               | [[https://adventofcode.com/2020/day/23][AoC 23]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day23.py][day 23]]   |
| Day 24: Lobby Layout            | [[https://adventofcode.com/2020/day/24][AoC 24]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day24.py][day 24]]   |
| Day 25: Combo Breaker           | [[https://adventofcode.com/2020/day/25][AoC 25]]    | [[https://gitlab.com/lbpierre/adventofcode-2k20/blob/master/days/day25.py][day 25]]   |

** Requirements
Install depencendies for tests and benchmarks:

~pip3 install -r requirement.txt~

** Help
Use the Makefile (_make_):
#+BEGIN_SRC shell
	 🎁 🎄 🎅 AdventOfCode 2020 🎅 🎄 🎁 

solve <day>:
		solve a specific day
solve-all:
		run all solutions of AOC 2020
test:
		run all test and coverage
bench:
		benchmark the code
bench-advanced:
		run an advanced benchmarch

#+END_SRC
