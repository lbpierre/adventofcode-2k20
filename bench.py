#!/usr/bin/python3

from profilehooks import profile

from aoc import solve_all


@profile
def bench_solve_all():
    solve_all()


if __name__ == "__main__":
    bench_solve_all()
